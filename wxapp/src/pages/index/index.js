import Taro, { Component } from '@tarojs/taro'
import { View, Image, Text, Button, ScrollView} from '@tarojs/components'
import './index.scss'
import banner from '../../assets/images/banner.png'
export default class Index extends Component {
  constructor () {
    super(...arguments)
    this.state = {
      hdgzShow: false
    }
  }
  /* 注意这里的参数顺序有点奇葩，只要有事件参数e必须放在最后面，否则报错；
   * 传参的时候必须要用bind，并且必须把this参数放在最前面
   */
  openHdgz = (e) => {
    e.stopPropagation();
    this.setState({
      hdgzShow: true
    })
  }
  closeHdgz = (e) => {
    e.stopPropagation();
    this.setState({
      hdgzShow: false
    })
  }
  render () {
    return (
      <View className='index'>
        <View className='banner'>
          <Image
            style='width: 100%;height: 100%;'
            src={banner}
          />
          <Text className='hdgz' onClick={this.openHdgz}>活动规则</Text>
        </View>
        <View className={this.state.hdgzShow?'hdgz-con show':'hdgz-con hide'}>
          <ScrollView
            className='scrollview'
            scrollY
            scrollWithAnimation
            scrollTop='0'
            style='height: 300px;'
            lowerThreshold='20'
            upperThreshold='20'
            onScrolltoupper={this.onScrolltoupper}
            onScroll={this.onScroll}>
            <View className='con-text'>
              H5 中 ScrollView 组件是通过一个高度（或宽度）
              固定的容器内部滚动来实现的，因此务必正确的设置容器的高度。
              例如: 如果 ScrollView 的高度将 body 撑开，就会同时存在两个滚动条（
              body 下的滚动条，以及 ScrollView 的滚动条）
              H5 中 ScrollView 组件是通过一个高度（或宽度）
              固定的容器内部滚动来实现的，因此务必正确的设置容器的高度。
              例如: 如果 ScrollView 的高度将 body 撑开，就会同时存在两个滚动条（
              body 下的滚动条，以及 ScrollView 的滚动条）
              H5 中 ScrollView 组件是通过一个高度（或宽度）
              固定的容器内部滚动来实现的，因此务必正确的设置容器的高度。
              例如: 如果 ScrollView 的高度将 body 撑开，就会同时存在两个滚动条（
              body 下的滚动条，以及 ScrollView 的滚动条）
              H5 中 ScrollView 组件是通过一个高度（或宽度）
              固定的容器内部滚动来实现的，因此务必正确的设置容器的高度。
              例如: 如果 ScrollView 的高度将 body 撑开，就会同时存在两个滚动条（
              body 下的滚动条，以及 ScrollView 的滚动条）
            </View>
          </ScrollView>
          <View className='hdgz-btn'>
            <Button size='mini' type='default' onClick={this.closeHdgz}>
              我知道了
            </Button>
          </View>
        </View>
      </View>
    )
  }
}
